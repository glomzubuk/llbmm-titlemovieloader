﻿using System.IO;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.Networking;

namespace TitleMovieLoader
{
    public class TitleMovieLoader
    {
        private static readonly string resourcePath = Path.Combine(Path.Combine(Application.dataPath, "Managed"), "TitleMovieLoaderResources");

        public static void PlayEditedVideo(ref string videoClipName, ref string url)
        {
            if(videoClipName != null && url == null && videoClipName == "BlazeLogo")
            {
                foreach (string file in Directory.GetFiles(resourcePath))
                {
                    if (Path.GetFileNameWithoutExtension(file) == "BlazeLogo")
                    {
                        videoClipName = null;
                        url = file;
                    }
                }
            }
        }
    }
}
