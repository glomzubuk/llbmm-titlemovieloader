
Just a little mod i developped for the friend AnkSoup

Drop a file named "BlazeLogo.webm" in the mod's resource folder to override the blaze video at the start.

For optimal compatibility, this file needs to be a webm with VP8 video encoding and VORBIS audio encoding, though you can get away with more on other platforms.
Refer to this link for more info: https://docs.unity3d.com/Manual/VideoSources-FileCompatibility.html
The file's extension doesn't matter, only the name does.

As with all mods that injects code with LLBMM, it is impossible to uninstall properly.
Though it does not alter the video if the file's not found.
